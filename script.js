var Y1 = document.getElementById('Y1');
var U = document.getElementById('U');
var K1 = document.getElementById('K1');
var K2 = document.getElementById('K2');
var Sdop = document.getElementById('Sdop');
var Svp = document.getElementById('Svp');
var N1 = document.getElementById('N1');
var N2 = document.getElementById('N2');
var T = document.getElementById('T');
var H = document.getElementById('H');

var Y2 = document.getElementById('Y2');
var Et = document.getElementById('Et');
var Syc = document.getElementById('Syc');
var Sp = document.getElementById('Sp');
var Hp = document.getElementById('Hp');
var Hk = document.getElementById('Hk');
var Vt = document.getElementById('Vt');
var Hvo = document.getElementById('Hvo');

var y1, u, k1, k2, sdop, svp, n1, n2, t, h;

var y2, et, syc, sp, hp, vt, hk, hvo;

var cnv = document.getElementById('cnv');

function calc() {
	y1 = parseFloat(Y1.value);
	u = parseFloat(U.value);
	k1 = parseFloat(K1.value);
	k2 = parseFloat(K2.value);
	sdop = parseFloat(Sdop.value);
	svp = parseFloat(Svp.value);
	n1 = parseFloat(N1.value);
	n2 = parseFloat(N2.value);
	t = parseFloat(T.value);
	h = parseFloat(H.value);

	y2 = y1 * Math.pow((1 + u / 100), t);
	et = (((y1 + y2) / 2) * ((n1 + n2) / 2) * t * (k2 / k1));
	syc = 3 * et / h;
	sp = 1.1 * syc + sdop;
	hp = (3 * et) / (syc + svp + Math.sqrt(syc * svp));
	vt = et * (1 - 1 / k2);
	hk = 1.1 * vt / syc;
	hvo = hp - hk + 1;

	Y2.innerHTML = y2.toFixed(1);
	Et.innerHTML = et.toFixed(1);
	Syc.innerHTML = syc.toFixed(1);
	Sp.innerHTML = sp.toFixed(1);
	Hp.innerHTML = hp.toFixed(1);
	Hk.innerHTML = hk.toFixed(1);
	Vt.innerHTML = vt.toFixed(1);
	Hvo.innerHTML = hvo.toFixed(1);
}

function draw() {
	calc();
	var context = cnv.getContext("2d");
	var width = cnv.width;
	var centerX = width/2;
	var height = cnv.height;
	context.clearRect(0,0,cnv.width,cnv.height);

    context.beginPath();
    for (var x = 0.5; x < width; x += 10) {
		context.moveTo(x, 0);
		context.lineTo(x, height);
	}
	context.moveTo(width-0.5, 0);
	context.lineTo(width-0.5, height);
	for (var y = 0.5; y < height; y += 10) {
		context.moveTo(0, y);
		context.lineTo(width, y);
	}
	context.moveTo(0, height);
	context.lineTo(width, height);
	context.strokeStyle = "#eee";
	context.stroke();

	var np = Math.sqrt(sp);
	var vl = Math.sqrt(svp)/np * centerX;
	var nl = Math.sqrt(sdop)/np * centerX;
	var lhp = hp / np * height * 9;
	var lhk = hk / np * height * 9;
	np = centerX;
	console.log("vl= " + vl);
	console.log("nl= " + nl);
	console.log("hp= " + hp);
	console.log("hk= " + hk);
	console.log("np= " + np);
	//1
	context.beginPath();
	context.lineWidth = 10;
	context.moveTo(centerX, 5);
	context.lineTo(centerX-vl, 5);
	//2
	context.moveTo(centerX, lhp);
	context.lineTo(centerX-np, lhp);
	//3
	context.moveTo(centerX-vl,5);
	context.lineTo(centerX-nl,lhp);
	//4
	context.moveTo(centerX-((nl-vl)/2+vl), lhp/2);
	context.lineTo(centerX, lhp/2);
	//5
	context.strokeStyle = "#000";
	context.stroke();
	context.beginPath();
	context.lineWidth = 2;
	context.moveTo(centerX-nl,lhp+lhk);
	context.lineTo(centerX, lhp+lhk);
	//6
	context.moveTo(centerX-nl,lhp);
	context.lineTo(centerX-nl,lhp+lhk);

	//1
	context.strokeStyle = "#000";
	context.stroke();
	context.beginPath();
	context.lineWidth = 10;
	context.moveTo(centerX, 5);
	context.lineTo(centerX+vl, 5);
	//2
	context.moveTo(centerX, lhp);
	context.lineTo(centerX+np, lhp);
	//3
	context.moveTo(centerX+vl,5);
	context.lineTo(centerX+nl,lhp);
	//4
	context.moveTo(centerX+((nl-vl)/2+vl), lhp/2);
	context.lineTo(centerX, lhp/2);
	//5
	context.strokeStyle = "#000";
	context.stroke();
	context.beginPath();
	context.lineWidth = 2;
	context.moveTo(centerX+nl,lhp+lhk);
	context.lineTo(centerX, lhp+lhk);
	//6
	context.moveTo(centerX+nl,lhp);
	context.lineTo(centerX+nl,lhp+lhk);

	context.strokeStyle = "#000";
	context.stroke();	
}

function validation(that) {
	that.value = that.value.replace(/[^\d.,]+/gi, '').replace(/^z+/, '')
}

function clear() {
	Y1.value = "";
	U.value = "";
	K1.value = "";
	K2.value = "";
	Sdop.value = "";
	Svp.value = "";
	N1.value = "";
	N2.value = "";
	T.value = "";
	H.value = "";
}